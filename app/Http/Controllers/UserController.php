<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('users');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required|string|min:8',
        ]);

        User::create($request->all());

        return redirect()->back()->with(['success' => 'true', 'title' => 'Excelente', 'text' => 'El usuario ha sido creado con exito']);
    }

    public function show($id)
    {
        $user = User::withTrashed()->whereId($id)->first();
        return view('user', compact('user'));
    }

    public function edit(User $user)
    {
        return response()->json(["user" => $user]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'username' => 'required',
            'password' => 'required|string|min:8',
        ]);

        $user->update($request->all());

        return redirect()->back()->with(['success' => 'true', 'title' => 'Excelente', 'text' => 'El usuario ha sido actualizado con exito']);
    }

    public function destroy (Request $request, User $user)
    {
        if ($request->ajax()) {
            $user->delete();
            $users = User::orderBy('id', 'DESC')->paginate(5);

            return response()->json(["users" => $users]);
        }
    }

    public function restore($id)
    {
        $user = User::withTrashed()
            ->whereId($id)
            ->firstOrFail();
        $user->restore();

        return redirect()->back();
    }

    public function getUsers(Request $request, User $user)
    {
        $data = $user->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {
                return "<a class='btn btn-sm btn-primary' href='/users/$data->id}'>Ver</a>
                <button class='btn btn-sm btn-primary' onclick='getUser( $data->id )'>Editar</button>
                <button type='button' class='btn btn-sm btn-danger' onclick='deleteData( $data->id )'>Borrar</button>";
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
}

