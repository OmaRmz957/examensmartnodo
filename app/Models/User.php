<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $with = ['courses', 'details'];

    public function getData()
    {
        return static::orderBy('created_at', 'desc')->get();
    }

    public function courses()
    {
        return $this->hasMany(UsersSubject::class);
    }

    public function details()
    {
        return $this->hasOne(UsersDetail::class);
    }

    public function getFullNameAttribute()
    {
        return ucwords($this->name).' '.ucwords($this->last_name);
    }
}
