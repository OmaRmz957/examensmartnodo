<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersSubject extends Model
{
    use HasFactory;

    protected $table = "school_subjects";
}
