<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('css')
        @livewireStyles
    </head>

    <body class="app">

        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="{{ route('index') }}">Examen</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="mr-auto navbar-nav">
                    <li class="nav-item {{ request()->is('index', '/') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('index') }}">Inicio</a>
                    </li>

                    <li class="nav-item {{ request()->is('users') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('users.index') }}">Usuarios</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container px-4 py-5 col-xl-10 col-xxl-8">

            <div class="py-5 row align-items-center g-5">
                <div class="text-center col text-lg-start">
                  <h1 class="mb-3 display-4 fw-bold lh-1">Examen</h1>
                  <p class="col-lg-10 fs-4">
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi aliquid perferendis saepe porro debitis vel corporis voluptate fuga, cupiditate animi necessitatibus a exercitationem adipisci error modi assumenda? Distinctio, amet laudantium.
                    </p>
                </div>
            </div>

            <hr>

            <div class="py-5 row align-items-center g-5">

                @yield('content')

            </div>
        </div>

        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script>
            @if(session('success'))
                Swal.fire({
                    title: '{{ session('title') }}',
                    text: '{{ session('text') }}',
                    icon: 'success',
                    timer: '2500'
                })
            @endif
        </script>
        @yield('js')
        @livewireScripts
    </body>
</html>
