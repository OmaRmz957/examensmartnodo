@extends('welcome')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
@endsection

@section('content')
    <div class="col-12">
        <h1 class="display-4">Mostrar Usuario</h1>
    </div>

    <div class="p-4 pb-0 shadow-lg pe-lg-0 pt-lg-5 align-items-center rounded-3 col-12">
        <div class="p-3 col p-lg-5 pt-lg-3">
          <h1 class="display-4">{{ $user->full_name }}</h1>

            <p class="lead">
                estatus:
                <span class="text-muted">{{ $user->deleted_at ? 'inactivo' : 'activo' }}</span>
            </p>

            <p class="lead">
                email:
                <span class="text-muted"> {{ $user->email }} </span>
            </p>

            <p class="lead">
                nombre de usuario:
                <span class="text-muted">{{ $user->username }}</span>
            </p>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                      Informacion de la escuela
                    </button>
                  </h2>

                  <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        @if($user->details)
                            <p class="lead">
                                Grupo:
                                <span class="text-muted">{{ $user->details->group }}</span>
                            </p>

                            <p class="lead">
                                Grado:
                                <span class="text-muted">{{ $user->details->grade }}</span>
                            </p>
                        @else
                        No hay informacion que mostrar
                        @endif
                    </div>
                  </div>
                </div>

                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                      Materias cursadas
                    </button>
                  </h2>

                  <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body">
                        @forelse($user->courses ?: [] as $course)
                        <p class="lead">
                            Materia:
                            <span class="text-muted">{{ $course->name }}</span>
                        </p>
                        @empty
                        No hay informacion que mostrar
                        @endforelse
                    </div>
                  </div>
                </div>
              </div>

              @if($user->deleted_at)
              <a href="{{ route('restore', $user->id) }}" class="float-right btn btn-primary">Activar usuario</a>
              @endif

        </div>
      </div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
@endsection
