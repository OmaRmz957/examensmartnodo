<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="text-center modal-title w-100" id="exampleModalLabel">Registrar un nuevo usuario</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form class="p-5 rounded-3" method="POST" id="usersModal" action="{{ route('users.store') }}">
                    <input name="_method" id="methodForm" type="hidden" value="POST">
                    @csrf
                    <div class="mb-3 form-floating">
                        <input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}" required>
                        <label for="floatingInput">Nombre</label>
                        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>

                    <div class="mb-3 form-floating">
                        <input type="text" class="form-control" placeholder="Apellido" name="last_name" value="{{ old('last_name') }}">
                        <label for="floatingInput">Apellido</label>
                        @error('last_name') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>

                    <div class="mb-3 form-floating">
                    <input type="email" class="form-control" placeholder="name@example.com" name="email" value="{{ old('email') }}" required>
                    <label for="floatingInput">Email</label>
                        @error('email') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>

                    <div class="mb-3 form-floating">
                        <input type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" required>
                        <label for="floatingInput">Nombre de usuario</label>
                        @error('username') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>

                    <div class="mb-3 form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Contraseña" name="password" required>
                    <label for="floatingPassword">Password</label>
                    @error('password') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>

                    <div class="form-floating">
                        <button type="button" class="btn btn-secondary w-100 col-5 btn-md-lg" data-dismiss="modal">Cancelar</button>

                        <button class="float-right w-100 btn btn-primary col-5 btn-md-lg" type="submit">Guardar</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
