@extends('welcome')

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
@endsection

@section('content')
    <div class="mb-4 col col-md-3">
        <button type="button" class="btn btn-primary btn-lg" id="btnUser" data-toggle="modal" data-target="#exampleModal">
            Crear nuevo usuario
        </button>
    </div>

    <div class="col-12">
        <h3>Lista de usuarios</h3>

        <div class="table-responsive">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                        <th width="150" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody id="users">
                </tbody>
            </table>
        </div>
    </div>

    @include('modal')

    @endsection

    @section('js')
    {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {{-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $.noConflict();
            var dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                pageLength: 5,
                // scrollX: true,
                "order": [[ 0, "desc" ]],
                ajax: '{{ route('get-users') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'Actions', name: 'Actions', orderable:false, serachable:false, sClass:'text-center'},
                ]
            });
        });

        async function getUser(id) {
            var url = `/users/${id}/edit`

            axios.get(url)
            .then(response => {
                document.getElementById('btnUser').click();

                $("#usersModal").attr('action', `/users/${id}` )
                $("#methodForm").attr('value', `PUT`)

                $('input[name=name]').val(response.data.user.name)
                $('input[name=last_name]').val(response.data.user.last_name)
                $('input[name=email]').val(response.data.user.email)
                $('input[name=username]').val(response.data.user.username)
            }).catch(e => {
                console.log(e);
            });
        }

        async function deleteData(id) {
            var url = `/users/${id}`

            axios.delete(url, {
                user: id,
            }).then(response => {
                const table = $("#users").empty();

                $.each(response.data.users.data, function(key, value) {
                  table.append(`
                  <tr>
                        <th scope="row">${ value.id }</th>
                        <td>${ value.name }</td>
                        <td>${ value.last_name }</td>
                        <td>${ value.email }</td>
                        <td>
                            <a class='btn btn-sm btn-primary' href='/users/${value.id}'>Ver</a>
                            <button class='btn btn-sm btn-primary' onclick='getUser( ${value.id} )'>Editar</button>
                            <button type="button" class="btn btn-sm btn-danger" onclick="deleteData(${ value.id })">Borrar</button>
                        </td>
                    </tr>
                  `)
                })

                Swal.fire({
                    title: 'Excelente',
                    text: 'El usuario ha sido eliminado correctamente',
                    icon: 'success',
                    timer: '1500'
                })

            }).catch(e => {
                console.log(e);
            });
        }

        @if( $errors->any() )
            $(document).ready(() => {
                document.getElementById('btnUser').click();
            });
        @endif
    </script>
@endsection
