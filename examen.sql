-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: examen_smartnodo
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `school_subjects`
--

DROP TABLE IF EXISTS `school_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `school_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_subjects_user_id` (`user_id`),
  CONSTRAINT `school_subjects_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_subjects`
--

LOCK TABLES `school_subjects` WRITE;
/*!40000 ALTER TABLE `school_subjects` DISABLE KEYS */;
INSERT INTO `school_subjects` VALUES (1,'Maths',21,NULL,NULL,NULL),(2,'Espanol',21,NULL,NULL,NULL);
/*!40000 ALTER TABLE `school_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `username` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ms. Adrianna McGlynn','Corwin','altenwerth.lucius@example.org','hahn.vito','$2y$10$we3mexe8xCEVwokw.JcNgusiBRHVSy89wDlNFFP7oc3VlCMwnzfay','2021-04-16 03:25:52','2021-04-16 11:02:28',NULL),(2,'Bonita Lehner','Nienow','mccullough.ellis@example.com','ocassin','$2y$10$eV4uqLKYFsg0P1fQi3TFSO5I6f7ISRwSF7dQFAxFmJSisMNOVQFBa','2021-04-16 03:25:52','2021-04-16 11:01:55',NULL),(3,'Tianna Braun','Rolfson','ugoodwin@example.net','morar.leland','$2y$10$k8IMobga8kHlvG/V5J.6NuCYsNNPlrcvwqTk53ahHbYFnj5JBQwMW','2021-04-16 03:25:52','2021-04-16 07:16:47',NULL),(4,'Helga Lubowitz','Cruickshank','iyundt@example.net','mreynolds','$2y$10$LCKcxAZZCSE4ZKHoHHme5eMcBOCULcnFnA3ak1Ip2iM1yXdwKJ5Fu','2021-04-16 03:25:52','2021-04-16 07:12:51',NULL),(5,'Prof. Ignatius Wisozk','Crooks','agoodwin@example.org','cody.rippin','$2y$10$Vyo7cLet.tqcIj29m9yXgOZyFTKjfDmxJFxS68UOEfhMnCop6YH6i','2021-04-16 03:25:52','2021-04-16 07:12:50',NULL),(6,'Jacinthe Jenkins III','Lueilwitz','oral.terry@example.org','orath','$2y$10$AgUU6zaXb2391.uuPy7LBeeVbdO/RQJF.M1tv06tTe3E5yY35wJha','2021-04-16 03:25:52','2021-04-16 07:12:48',NULL),(7,'Dr. Rene Kohler DDS','Parisian','olegros@example.org','okris','$2y$10$gSreom03xRBI37ogpQJQpOk7/xVrX6Wde/b5yvvKpPXiWJNA3o1rK','2021-04-16 03:25:52','2021-04-16 07:12:45',NULL),(8,'Nikolas Morar','Mohr','adams.lucas@example.org','queen.rogahn','$2y$10$Cm/T6Q2Zh8H70/4T/DwmYOOLVxbkk4l3O9fWWzKYnlHC10FBTDUgO','2021-04-16 03:25:52','2021-04-16 06:05:13',NULL),(9,'Miss Christy Flatley DVM','Keeling','marlen67@example.com','schuppe.lisette','$2y$10$VMzgOPhKSuw7DW5Kwirj/uKbzeXvHHimAyuU0yLKAdNQp5hclh0o.','2021-04-16 03:25:52','2021-04-16 05:59:31',NULL),(10,'Adam Mayert V','O\'Hara','cdavis@example.com','lukas61','$2y$10$rdd/VM/MThtowCDRGsQUte.VFKTJkU85WoL.DAQst4mjt5HQkVEBi','2021-04-16 03:25:52','2021-04-16 05:57:55',NULL),(11,'Prof. Christopher Krajcik','Kiehn','ibrahim40@example.com','sven51','$2y$10$68H98wWcX7TZ0WcujYYRFeZ5s0Qc1.8SAAXXUBesW0FLvPGlr0vm6','2021-04-16 07:17:16','2021-04-16 11:04:03',NULL),(12,'Giles McDermott','Hyatt','bobby59@example.com','vgrimes','$2y$10$M5PnKWZChhArOrt71euBYeUVjtNqBVPfGbpks.CuB4ykbJ4WwP1q6','2021-04-16 07:17:16','2021-04-16 07:17:16',NULL),(13,'Calista Runolfsdottir','Jast','casper.camille@example.com','trantow.demarcus','$2y$10$IQYI53na9mnYa/zfpM3Kve4ZbT2KzsWqYD5iNf49zts7wsIiY6Ive','2021-04-16 07:17:16','2021-04-16 07:33:56',NULL),(14,'Elian Kuhlman','Steuber','andreanne46@example.net','balistreri.ellen','$2y$10$.XPBrZqW5BF03nMBb4eGXeSH1mtNECEo5Jp/d64HXqzRECnaVqFr6','2021-04-16 07:17:16','2021-04-16 07:33:52',NULL),(15,'Prof. Mable Cummings MD','Sporer','providenci.harris@example.net','toby.reichert','$2y$10$o6zXairmoI3gpOgB/JpNouE7WslILzw7Ka6mCk82RBtUmRzV3vD/K','2021-04-16 07:17:16','2021-04-16 07:19:21',NULL),(16,'Prof. Giuseppe DuBuque PhD','Kirlin','micah.oreilly@example.net','mafalda.lynch','$2y$10$Ofl4WRKZ.aOmqm1c6RhjL.eLO9xIYOvFDIX6/i/J9l3KLypAEZWcC','2021-04-16 07:17:16','2021-04-16 07:19:16',NULL),(17,'Shanna Berge','Kuphal','adriana.herman@example.net','twuckert','$2y$10$Hlh0DThJdR7DnKUVhmxIFeEjb/BOQDv5ySXqn/hpMU7uMTW.KIO5y','2021-04-16 07:17:16','2021-04-16 07:19:06',NULL),(18,'Mr. Jennings Marks','Homenick','zbins@example.net','wgusikowski','$2y$10$h2IFD3IV99Nnl55HZ5CcAOLnj3eRuIKecaza8Tn.EGZuleddpDRxK','2021-04-16 07:17:16','2021-04-16 07:18:50',NULL),(19,'Ricky Hill','Rau','betty72@example.org','zulauf.meghan','$2y$10$n2NYnVRUbHmRqeueDKfTZeQF7o3uRIYF0y1QxWWmG5ck7qkuK8Pru','2021-04-16 07:17:16','2021-04-16 12:16:45','2021-04-16 12:16:45'),(20,'Justice Stroman V','Marks','andre39@example.com','richard23','$2y$10$GMWlyyr/k6OzzS1lBGbs0ubYXvdygOXNpR.A9go3.qMU6pQNlrwfC','2021-04-16 07:17:16','2021-04-16 12:02:29','2021-04-16 12:02:29'),(21,'Omar','alalala','omar29957@gmail.com','OmaRmz','123123123','2021-04-16 08:57:56','2021-04-16 12:00:21','2021-04-16 12:00:21'),(22,'alejandro','alalala','client@client.com','OmaRmz3','33333333123','2021-04-16 09:05:26','2021-04-17 01:18:14','2021-04-17 01:18:14'),(23,'Omar','abap','omar299575@gmail.com','OmaRmz12','1231231233','2021-04-16 10:18:50','2021-04-16 12:00:18','2021-04-16 12:00:18'),(24,'Prueba','awerr','client@client.com12323','OmaRmz221','1212323323','2021-04-16 12:04:47','2021-04-16 12:14:43',NULL),(25,'Omar','a','omar@homeosystems.com','omar111','123123123123','2021-04-16 12:11:28','2021-04-16 12:11:28',NULL),(26,'Omar','a','omar299573333@gmail.com','OmaRmz123','1231231232313123','2021-04-16 12:16:15','2021-04-16 12:16:41','2021-04-16 12:16:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_details`
--

DROP TABLE IF EXISTS `users_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `grade` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_details_user_id` (`user_id`),
  CONSTRAINT `users_details_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_details`
--

LOCK TABLES `users_details` WRITE;
/*!40000 ALTER TABLE `users_details` DISABLE KEYS */;
INSERT INTO `users_details` VALUES (1,21,'A','5',NULL,NULL);
/*!40000 ALTER TABLE `users_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-16 15:20:41
